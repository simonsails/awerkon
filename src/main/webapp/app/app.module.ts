import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AwerkonSharedModule } from 'app/shared/shared.module';
import { AwerkonCoreModule } from 'app/core/core.module';
import { AwerkonAppRoutingModule } from './app-routing.module';
import { AwerkonHomeModule } from './home/home.module';
import { AwerkonEntityModule } from './entities/entity.module';
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    AwerkonSharedModule,
    AwerkonCoreModule,
    AwerkonHomeModule,
    AwerkonEntityModule,
    AwerkonAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class AwerkonAppModule {}
