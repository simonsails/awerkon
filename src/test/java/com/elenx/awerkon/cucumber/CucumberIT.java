package com.elenx.awerkon.cucumber;

import com.elenx.awerkon.AbstractNeo4jIT;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", features = "src/test/features")
//@ExtendWith(AbstractNeo4jIT)
public class CucumberIT {}
